Rest API [JSON based]




Meta:
 the protocol version is also getting transmitted,...

```
{
 "meta": {
  "protocol-version": 1,                                # version of the protocol; required
  "version": 1                                          # version of the server/client; required

 }


}
```

 
Commands: 
 In the root json is a json array called data. in this element there are all commands and data for the rest api. see command for more. there is currently no limit for commands
```
{
 "data": {
 "0": {
  }
 }
}
```

Command:
 Every command has an unique id. See Commands.md for more info
```
{
 "id": 0,                                                    # id: per request the id starts at 0 and increses per 1 with every command.
 "command": 1,                                               # command id; required
 "data": {                                                   # requested data. depends on command id
 }
}
```

Responses:

Once a command has been sent, the other side will send a response according to
```
{
 "response": {
  "0":{
  }
 }
}
```

Response:
```
{
 "id": 0,                                                    # id: per response the id starts at 0 and increses per 1 with every response.
 "command": 1                                                # command id; required
}
```
