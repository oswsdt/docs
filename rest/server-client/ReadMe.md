Server-Client commands

```
Command-ID: 0
Name: SET_STATUS_ON_CLIENT
Description: Sends an status update to a client
Comment: Must get orderd by somebody
Response: status update
Data-parameters:
 status: check for repo update (coming soon) => 0, shutdown => 1, reboot => 2, logoff specific user => 3, log off all clients => 4, lock => 5, gpupdate => 6, log off current client => 7
 force: Forces the specific action: true/false
Dependend paramenters:
 status=2
  user: DOMAIN\USERNAME
```
```
Command-ID: 1
Name: RET_SOFTWARE_REPOSITORY
Description: Returns all software, which is available to instal / should be installed on the specific device
Comment: Must get requested by device
Data-parameters:
 json array like:
 [
    {
        "title": "Open Shell",
        "author": "The Open Shell Team",
        "wname": "Open-Shell",
        "wauthor": "The Open-Shell Team",
        "wnameformat": "%wname%",
        "version": "4.4.142",
        "path": "R:\\OpenShellSetup_4_4_142.exe",
        "parameters": "/qn ADDLOCAL=StartMenu"
    }
 ]
```