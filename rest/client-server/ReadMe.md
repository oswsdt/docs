Client to Server commands



### General Info
```
Command-ID: 0
Name: SEND_GENERAL_INFO_TO_SERVER
Description: Send Meta Data from the client to the server
Comment: Is getting sent, once the connection is established.
Response: None
Data-parameters:
 os: OS Type (platform.platform()): Windows-10-10.0.18362-SP0
 architecture: System Architecture: 0 -> x86, 1-> x64, 2 -> arm
 device-name: Device Name (string): laptop-moritz
 memory: System Memory in MB: 8192
 domain: Domain (string): TESTDOMAIN
```


```
Command-ID: 1
Name: SEND_STATUS_INFO_TO_SERVER
Description: Sends current status info to server
Comment: Periodically every 10 secs
Response: Currently None
Data-parameters:
 status: installing => 0, locked => 1, used => 2, idle => 3, shutting down => 4, logging off => 5
 sessions: List of DOMAIN\USERNAME split by ","
 active-session: DOMAIN\USERNAME or None

```


```
Command-ID: 2
Name: GET_SOFTWARE_REPOSITORY
Description: Requests the software repository (what the client should install)
Comment: When the server requests it/on start
Response: server->client:commandid=2
```