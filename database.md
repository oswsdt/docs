# Database layout





```
## Applications
```
id: id of the application (1)
name: application name (Open Shell)
Author: Author of the Application (The Open Shell Team)
wname: Only the Name shown in windows control panel (if there are other strings at the end like the version, leave it out here): (Open-Shell)
wauthor: The author or company shown in the windows control panel: (The Open-Shell Team)
wnameformat: Format of the application name: see Application name for details (1)
installer: Installer id: (1)
```

## application_version
```
id: id of the version: (1)
application: application id: (1)
architecture: x86/x64/arm: (x64)
version: application version (shown in windows control panel): 4.4.142
path: Path of installer. Must be accessible from all devices!: (R:\OpenShellSetup_4_4_142.exe)
```

## Auth
```
id: client id
cert: pub key of client
```

## Devices
```
id: id of the device (=clients->id) (1)
name: device name (MyPC)
os: python: platform.platform(): Windows-10-10.0.18362-SP0
architecture: x86/x64/arm: (x64)
ram: memory size in mb (8192)
domain: domain name (TESTDOMAIN)
groups: list of groups (1, 2, 3)

## Groups
```
id: id of the group: (1)
name: name of the group: (Devices)
description: Group Description: (All Devices)
```

## Installer
```
id: id of the installer: (1)
name: Name of the installer
parameter: parameter ids split by "," (1,2,3)
```

## Parameter
```
id: id of the parameter: (1)
name: Name of the parameter: (Silent Install)
description: Description of the parameter: (Runs the installer without user interfaction)
value: The parameter itself: (/q)
```

## Rollouts
```
id: id of the rollout/deployment: (1)
application: Application id
version: Version of the application
group: group ids split by ","
```

## wname formats
```
id: id of the rollout/deployment: (1)
format: Format of the name in windows control panel(Vars: %version%, %name%): (%name% %version% (64bit))
```

